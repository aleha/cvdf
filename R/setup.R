##' Create Structure for Cross Validations
##'
##' Create the structure for a cross validation by generating the
##'   indices for the training and test sets to be used throughout the
##'    cross validation.
##' @param vec categorical vector.  Typically the response vector of a
##'   classification problem
##' @param n numeric.  number of samples.  Ignored (with a warning) if
##'   \code{vec} is given
##' @param nreps numerical.  the number of repeats of the cross
##'   validation to generate
##' @param nfolds numerical.  the number of folds to generate within
##'   each repeat
##' @param stratified logical.  if \code{TRUE} (and \code{vec} is
##'   given) all generated (training and test) sets will
##'   (approximately) share the distribution of elements with the full
##'   \code{vec}.
##' @param verbose numeric.  level of verbosity with \code{verbose =
##'   0} means to not generate any output.
##' @return data.frame with additional class 'cvdf'.
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' ## 10 times repeated 10 fold cross validation on 50 samples:
##' setupCVDF(n = 50)
##'
##' ## generate response vector
##' vec <- sample(c("tumor", "control"), 55, replace = TRUE, prob = c(1, 3))
##' ## generated cross validation scheme
##' cvdf <- setupCVDF(vec)
##' ## generated cross validation scheme -- this time stratified
##' cvdf <- setupCVDF(vec, stratified = TRUE)
setupCVDF <- function(vec, n, nreps = 10, nfolds = 10, stratified = FALSE, verbose = 0)
{
  ## check arguments
  if (!missing(vec)) {
    if (!missing(n))
      warning(paste0("'n' is ignored if 'vec' is given; setting 'n = ", length(vec), "'"))
    n <- length(vec)
  } else {
    if (stratified)
      stop("'stratified = TRUE' is only possible if 'vec' is given.")
  }

  ## initiate the cvdf
  ## one entry per repeat
  cvdf <-
    data.frame(rep = 1:nreps)
  if (verbose) cvdf %>% print

  ## draw a permutation for each reapeat
  if (!stratified) {
    cvdf <- cvdf %>%
      group_by_("rep") %>%
      do(rep_perm = sample(1:n, n, replace = FALSE))
  } else {
    cvdf <- cvdf %>%
      group_by_("rep") %>%
      mutate_(.dots = setNames(list(~list(permuteWithinAndOrder(vec))), "rep_perm"))
      ##do(rep_perm = permuteWithinAndOrder(vec))
  }
  if (verbose) cvdf %>% print
  if (verbose) cvdf %>% dplyr::filter(rep == 1) %>% select(one_of("rep_perm")) %>% unlist(recursive = FALSE) %>% '[['(1)
  if (verbose && !missing(vec)) vec[(cvdf %>% dplyr::filter(rep == 1) %>% select(one_of("rep_perm")) %>% unlist(recursive = FALSE) %>% '[['(1))]
  if (verbose) cvdf %>% dplyr::filter(rep == 2) %>% select(one_of("rep_perm")) %>% unlist(recursive = FALSE) %>% '[['(1)
  if (verbose && !missing(vec)) vec[(cvdf %>% dplyr::filter(rep == 1) %>% select(one_of("rep_perm")) %>% unlist(recursive = FALSE) %>% '[['(1))]

  cvdf <- cvdf %>% group_by(rep) %>% do({
    cvrow <- .
    rep_perm <- cvrow %>% .$rep_perm %>% '[['(1)

    ## initiate data.frame with fold numbers
    folddf <- data.frame(fold = 1:nfolds)

    ## add test and training indices
    ## by going over the samples in steps of size 'nfolds' the groups are
    ## - balanced in size
    ## - stratified by 'vec' (as the permuteWithinAndOrder ensures the
    ##   permutation to be ordered by 'vec')
    folddf <- folddf %>% group_by_("fold") %>% do({

      ## extract fold
      foldrow <- .
      fold <- foldrow$fold[1]

      ## compute the test index
      idx.test <- seq(fold, n, by = nfolds)
      idx.test <- rep_perm[idx.test]
      ## setdiff to arrive at the train index
      idx.train <- setdiff(1:n, idx.test)
      ##idx.train <- rep_perm[idx.train]

      ## put into data.frame
      data.frame(idx_test = I(list(idx.test)),
                 idx_train = I(list(idx.train)))
    })

    ## put indices from all folds together
    data.frame(rep_perm = I(alply(1:nfolds, 1, function(x) rep_perm)), folddf)
  })

  ## add vec
  if (!missing(vec)) {
    cvdf <- cvdf %>%
      ungroup %>%
      mutate_(.dots = setNames(list(~rep(list(vec), nrow(cvdf))), "vec"))
  }

  ## add stratified
  attr(cvdf, "stratified") <- stratified

  ## ungroup
  cvdf <- cvdf %>% ungroup

  ## class
  class(cvdf) <- c("cvdf", class(cvdf))

  ## end
  return(cvdf)
}



## Randomly Permute the Places of Identical Elements in a Vector
##
## This is a mere helper function which randomly permutes the indices
##   of identical elements of a vector.  Additionally the permuted
##   indices are ordered such that the vector at these positions will
##   put identical elements next to each other.
## @param vec the vector to permute
## @return numeric vector of permuted indices
## @author Dr. Andreas Leha
## @examples
## ## generate response vector
## vec <- sample(c("tumor", "control"), 55, replace = TRUE, prob = c(1, 3))
##
## ## permuted index
## ind <- cvdf:::permuteWithinAndOrder(vec)
## ind
##
## ## check:
## vec[ind]
permuteWithinAndOrder <- function(vec)
{
  ## put vec as first colum in data.frame
  permdf <- data.frame(x = vec)

  ## add the initial index as second column
  permdf <- permdf %>% mutate_(.dots = setNames(list(~(1:n())), "i"))

  ## permute the index by the elements of vec
  permdf <-
    permdf %>%
    group_by_("x") %>%
    mutate_(.dots = setNames(list(~sample(i, n(), replace = FALSE)), "iperm"))

  ## test
  if (FALSE)
    permdf %>% group_by_("x") %>% summarize_(~identical(sort(i), sort(iperm)))

  ## order by vec
  permdf <- permdf %>% arrange_(~x)

  ## end
  return(permdf$iperm)
}
